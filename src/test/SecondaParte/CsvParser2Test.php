<?php

use PHPUnit\Framework\TestCase;
use SecondaParte\CsvParser2;

class CsvParser2Test extends TestCase
{

	public function testWithoutMock()
	{

		$parser = new CsvParser2('prodotti.csv');

		$parser->getRows();
	}


	public function testGetRows()
	{

		$csvParserMock = $this->getMockBuilder(CsvParser2::class)
			->setConstructorArgs(array('prodotti.csv'))
			->getMock();

		$prod02 = array('prod02','simple','tavolo', 30);

		$rows = array();
		$rows[] = $prod02;

		$csvParserMock->method('getRows')
			->willReturn($rows);


		$this->assertEquals(1, count($csvParserMock->getRows()));

	}




}
