<?php

class ProductTest extends \PHPUnit\Framework\TestCase
{


	public function testLoad()
	{

		$pdoMock =$this->getMockBuilder(PDOMock::class)
			->setMethods(['prepare'])
			->getMock();

		$pdoStatementMock = $this->getMockBuilder('PDOStatement')
			->setMethods(['fetch'])
			->getMock();

		$pdoMock->method('prepare')->willReturn($pdoStatementMock);

		$product = new \SecondaParte\Product($pdoMock);
		$product->setPrice(10);


		$pdoStatementMock->expects($this->once())
			->method('fetch')
			->willReturn($product);


		$product->load(1);

		//var_dump($product);


	}

}