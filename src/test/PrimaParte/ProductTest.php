<?php

use PrimaParte\Product;

/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 2019-08-22
 * Time: 18:03
 */

class ProductTest extends \PHPUnit\Framework\TestCase
{


	public function getPriceList()
	{
		return array(
			array( "33", true),
			array( "29", false)
		);

	}

	/**
	 * @dataProvider getPriceList
	 */
	public function testProductHasFreeShipping($price, $result)
	{
		$product = new Product();

		$product->setPrice($price);

		$this->assertEquals($result, $product->hasFreeShipping() );
	}

}