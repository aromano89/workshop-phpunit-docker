<?php


use PrimaParte\InvalidFormatException;
use PrimaParte\Parser;


//require_once "../PrimaParte/InvalidFormatException.php";
/**
 * Class PrimaParteTest
 *
 * dimostrazione test delle exception
 * @backupGlobals enabled
 */

class PrimaParteTest extends \PHPUnit\Framework\TestCase
{


	public function testParseLine()
	{

		$parser = new Parser();
		$csvLine ="prod01;simple;Tavolo;33.00";

		$params = $parser->getParams($csvLine);

		$this->assertEquals(4, count($params));

		return $params;

	}


	/**
	 * @depends testParseLine
	 */
	public function testTypeIsSimple($params)
	{
		$parser = new Parser();

		$this->assertEquals('simple', $parser->getProductType($params));
	}



	public function testLineFormatIsNotValid() {

		$parser = new Parser();

		$csvLine ="prod01;simple;Tavolo ikea,33.00";

		$this->expectException(InvalidFormatException::class);

		$parser->getParams($csvLine);


	}


	public function testGetParamsUsingGlobals()
	{
		$GLOBALS['columnCount'] = 4;

		$parser = new Parser();

		$csvLine ="prod01;simple;Tavolo;33.00";

		$params = $parser->getParamsUsingGlobal($csvLine);

		$this->assertEquals(4, count($params));

	}





}