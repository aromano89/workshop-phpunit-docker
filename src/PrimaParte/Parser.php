<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 2019-07-09
 * Time: 18:20
 */

namespace PrimaParte;

class Parser
{

    const INDEX_SKU_COLUMN = 0;
    const INDEX_TYPE_COLUMN = 1;

    public function getParams($line)
    {
		$params = explode(';', $line);

		if (count($params) < 4) {
			throw new InvalidFormatException();
		}
		return $params;
    }


    public function getProductType($params)
    {
        return $params[self::INDEX_TYPE_COLUMN];
    }

    public function getParamsUsingGlobal($line)
	{
		global $columnCount;

		$params = explode(';', $line);

		if (count($params) != $columnCount) {
			throw new InvalidFormatException();
		}
		return $params;

	}


}