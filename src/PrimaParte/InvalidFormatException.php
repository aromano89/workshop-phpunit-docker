<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 2019-07-12
 * Time: 16:58
 */

namespace PrimaParte;


class InvalidFormatException extends \Exception
{

	/**
	 * InvalidFormatException constructor.
	 */
	public function __construct()
	{
	}
}