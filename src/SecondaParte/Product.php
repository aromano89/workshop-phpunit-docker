<?php
namespace SecondaParte;

class Product
{


    private $id;
    private $name;
    private $sku;
    private $price;

	/**
	 * @var \PDO
	 */
	private $db;

	public function __construct($db)
    {
		//$this->db = new \PDO('mysql:host=mariadb;dbname=mydb;charset=utf8', 'myuser', 'secret');
		$this->db = $db;
	}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

	public function hasFreeShipping()
	{
		if ($this->price>30)
			return true;

		return false;
	}


	public function load($productId) {

    	$query = "SELECT * FROM product WHERE id= :product_id";


    	$sth = $this->db->prepare($query);

    	$sth->execute(array(':product_id' => $productId));

    	$sth->setFetchMode(\PDO::FETCH_CLASS, Product::class);

    	$product = $sth->fetch();

    	//var_dump($product);
    	return $product;
	}
}