<?php
namespace SecondaParte;

class CsvParser2
{

    const INDEX_SKU_COLUMN = 0;
    const INDEX_TYPE_COLUMN = 1;

	public $csvRows = array();
	private $filename;

	public function __construct($filename)
	{
		$this->filename = $filename;
	}

	public function getRows()
	{

		$csv = fopen($this->filename,'r');

		while ( ($riga = fgetcsv($csv, 300,';')) !== false) {
			$this->csvRows[] = $riga;
		}

		fclose($csv);

		$mailer = new Mailer();
		$mailer->sendMail('alx.romano89@gmail.com','prova');

		return $this->csvRows;
	}

/*
	public function getParams($line)
    {
		$params = explode(';', $line);

		if (count($params) == 1) {
			throw new InvalidFormatException();
		}
		return $params;
    }


    public function getProductType($params)
    {
        return $params[self::INDEX_TYPE_COLUMN];
    }
*/

}