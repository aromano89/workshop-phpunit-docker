<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 2019-07-09
 * Time: 18:20
 */

namespace SecondaParte;

use PrimaParte\InvalidFormatException;

class CsvParser
{

    const INDEX_SKU_COLUMN = 0;
    const INDEX_TYPE_COLUMN = 1;

	private $csvRows = array();

	public function __construct($filename)
	{
		$csv = fopen($filename,'r');

		while ( ($riga = fgetcsv($csv, 120,';')) !== false) {
			$this->csvRows[] = $riga;
		}

		fclose($csv);
	}

	public function getRows(): array
	{
		return $this->csvRows;
	}



}